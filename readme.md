# todo: descomentar (cuando este listo) el **seeder manager** para evitar seeds en producción
# Configuración Básica
* crea un archivo `.env.local.php`
    * este archivo regresa un arreglo tipo *key-value* emulando *"variables de entorno"*
    * puedes acceder a las variables de entorno con la global **$_ENV['key];** ó con **getenv('key');**
   * más info [link](http://laravel.com/docs/configuration#environment-configuration)
