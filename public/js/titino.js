var imgArray = new Array();
var images = new Array();
var cont = 0;
var totalImages = 0;
var loadCounter = 0;
var time = 0;
var start = new Date().getTime();
var objects = JSON.parse(JSON.stringify(json)).data;

$(document).ready(function(){
	objects.reverse();
    loadImages();
    $(document).bind('everythingLoaded', function(e) {
        //alert("event_triggered");
        //hideLoadingScreen();
        startTime();
    });

    $.each(imgArray, function(i, item) {
        //alert("wupidobu " + item);
        item.image.onload = function() {
            //alert("loadCounter");
            incrementLoadCounter();
        }
    });
    //startTime();


});

/*function hideLoadingScreen() {
    $('#loadingScreen').hide();
    $('#anuncios').show();
}*/

function incrementLoadCounter() {
    loadCounter++;
    //alert(loadCounter);
    if(loadCounter === totalImages) {
        //alert("loadCounter is 5");
        $(document).trigger('everythingLoaded');
    }
}

function loadImages()
{
    /*for(var i = 0; i < 5; i++)
     {
     var img = new Image();
     img.src = "tuminuto/" + (i+1) + ".jpg";
     images[i] = img;
     }*/

    $.each( objects, function( i, val ) {
        images[i] = new Image();
        images[i].src = val.url;
        var prev = previousTime(i-1);
        imgArray[i] = {image: images[i], time: prev, duration: val.seconds};
    });
    totalImages = imgArray.length;


    /*imgArray = [
     { image: images[0], time: 0, duration: 12 },
     { image: images[1], time: 12, duration: 14 },
     { image: images[2], time: 25, duration: 15 },
     { image: images[3], time: 40, duration: 11 },
     { image: images[4], time: 51, duration: 9 }];*/
}

function previousTime(i)
{
    if(i<0)
        return 0;
    else
        return (imgArray[i].time + imgArray[i].duration);
}

function startTime()
{

    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    time = Math.round((new Date().getTime() - start)/1000);
// add a zero in front of numbers<10
    m=checkTime(m);
    s=checkTime(s);
    document.getElementById('txt').innerHTML=h + " " + m + " " + s + " " + cont + " " + time;
    t=setTimeout(function(){startTime()},500);

    if((imgArray[cont].time <= time) && ((imgArray[cont].time + imgArray[cont].duration) > time ))
    {
    	var img = imgArray[cont].image; 
    	var width = img.width;
    	var height = img.height;
    	var imgratio = width/height;
    	var windowratio = $(window).width()/$(window).height();
        document.getElementById("imageid").src=imgArray[cont].image.src;
        if(imgratio > windowratio)
    	{
    		//alert("gorda");
    		$("#imageid").css("width", "100%");
    		$("#imageid").css("height", "");
    	}
    	else
    	{
    		//alert(imgratio);
    		$("#imageid").css("height", "100%");
    		$("#imageid").css("width", "");
    	}
    }
    else
    {
        cont++;
        if(cont > (imgArray.length - 1))
        {
            cont = 0;
            start = new Date().getTime();
        }
    }
}

function checkTime(i)
{
    if (i<10)
    {
        i="0" + i;
    }
    return i;
}

/*Seccion tiempo real */
 Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };
var pusher = new Pusher('1c2ac6cdea9dda088413');
var channel = pusher.subscribe('titino_anuncio');

channel.bind('nuevo_anuncio', function(data) {
	document.getElementById("imageid").src="http://www.icapture.com/images/loading.gif";
	image = new Image();
    image.src = data.url;
    image.onload = function() {
	    var prev = previousTime(imgArray.length - 1);
	    imgArray.push({image: image, time: prev, duration: data.duration});
	    /*Siguientes 3 lineas es para impresionar, se comentan para que siga el orden normal*/
	    start = new Date().getTime() - (prev * 1000);
	    cont = imgArray.length - 1;
	    document.getElementById("imageid").src=imgArray[cont].image.src;
	    //alert("image: " + image.src + "time: " + prev + "duration: " + data.duration);
    }
});
