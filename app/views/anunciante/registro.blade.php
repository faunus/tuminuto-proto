<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <title>index</title>
    <link href="http://fonts.googleapis.com/css?family=Noto+Sans:400" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/test/css/standardize.css">
    <link rel="stylesheet" href="/test/css/index-grid.css">
    <link rel="stylesheet" href="/test/css/index.css">
</head>
<body class="body index clearfix">
<nav class="igator clearfix">
    <p class="brand">TU MINUTO</p>
</nav>
{{ Form::open([
    'route' => 'post.registro.anunciante',
    'class' => 'anunciante clearfix'
]) }}

    <!--    name-->
    @if( $errors->has('name') )
    <input class="name" name="name" style="border: brown 0.17em solid"
           placeholder="{{ $errors->first('name') }}" type="text">
    @else
    <input class="name" name="name"
           placeholder="Nombre Completo" type="text" value="{{ Input::old('name') }}">
    @endif

    <!--    phone-->
    @if( $errors->has('phone') )
    <input class="phone" name="phone" style="border: brown 0.17em solid"
           placeholder="{{ $errors->first('phone') }}" type="number">
    @else
    <input class="phone" name="phone"
           placeholder="Telefono / celular" type="number" value="{{ Input::old('phone') }}">
    @endif

    <!--    password-->
    @if( $errors->has('password') )
    <input class="password" name="password" style="border: brown 0.17em solid"
           placeholder="{{ $errors->first('password') }}" type="password">
    @else
    <input class="password" name="password"
           placeholder="Contraseña" type="password">
    @endif

    <!--    email-->
    @if( $errors->has('email') )
    <input class="email" name="email" style="border: brown 0.17em solid"
           placeholder="{{ $errors->first('email') }}" type="email">
    @else
    <input class="email" name="email"
           placeholder="Correo" type="email" value="{{ Input::old('email') }}">
    @endif

    <button class="registrarme" type="submit">Registrarme&nbsp;</button>
{{ Form::close() }}
</body>
</html>