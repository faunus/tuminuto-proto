{{ Form::open([
    "route"     => "anuncio.post",
    "method"    => "post",
    "files"     => true
])}}

{{ Form::label("ad_image", "Imagen") }}
<br/><br/>
{{ Form::file("ad_image") }}
<br/><br/>
{{ Form::select('ad_time', $timekinds_list, null) }}
<br/><br/>
{{ Form::select('ad_kind', $adkinds_list, null) }}
<br/><br/>
{{ Form::submit("sube tu ad") }}

{{ Form::close() }}