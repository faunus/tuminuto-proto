<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0">
	<title>home</title>
	<link href="http://fonts.googleapis.com/css?family=Noto+Sans:400,400" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/test/mobile/css/standardize
	.css">
	<link rel="stylesheet" href="/test/mobile/css/home-grid.css">
	<link rel="stylesheet" href="/test/mobile/css/home.css">
</head>
<body class="body home clearfix">

<nav class="igator clearfix">
	<img class="logo" src="/images/logo.png">
</nav>

<div class="new-ad clearfix">

	{{ Form::open(['route' => 'home.post', 'files' => true]) }}
	<label class="file file-1">Subir Anuncio</label>

	<div class="file file-2">
		<input style="width: 80%;" name="ad_image" type="file" id="input">
	</div>

	<!-- <select class="domain" name="ad-">
		<option value="Público de Interes">Público de Interes</option>
		<option value="Bares">Bares</option>
		<option value="Gimnasios">Gimnasios</option>
		<option value="Taquerías">Taquerías</option>
		<option value="Restaurantes">Restaurantes</option>
		<option value="Antros">Antros</option>
		<option value="Centros Comerciales">Centros Comerciales</option>
		<option value="Todos">Todos</option>
	</select> -->

	{{ Form::select('ad_location', $titinos_list, null, ["class" => "location"]) }}
	<!-- <select class="location" name="ad-location"> -->
		<!-- <option value="Ubicación">Ubicación</option> -->
		<!-- <option value="Norte">Norte</option> -->
		<!-- <option value="Norte">Norte</option> -->
		<!-- <option value="Sur">Sur</option> -->
		<!-- <option value="Centro">Centro</option> -->
	<!-- </select> -->

	<!-- <select class="schedule" name="ad-schedule">
		<option value="Horario de Exposición">Horario de Exposición</option>
		<option value="08:00 a 11:00">08:00 a 11:00</option>
		<option value="9:00 a 12:00">9:00 a 12:00</option>
		<option value="10:00 a 13:00">10:00 a 13:00</option>
		<option value="11:00 a 14:00">11:00 a 14:00</option>
		<option value="16:00 a 20:00">16:00 a 20:00</option>
		<option value="20:00 a 22:00">20:00 a 22:00</option>
		<option value="22:00 a 02:00">22:00 a 02:00</option>
	</select> -->

	<select class="kind" name="ad_kind">
		<option value="Tipo de Anuncio">Tipo de Anuncio</option>
		<option value="Adultos">Adultos</option>
		<option value="Deportes">Deportes</option>
		<option value="Ropa">Ropa</option>
		<option value="Venta General">Venta General</option>
		<option value="Electrónicos">Electrónicos</option>
		<option value="Instrumentos Musicales">Instrumentos Musicales</option>
		<option value="Infantil">Infantil</option>
		<option value="Otros">Otros</option>
	</select>

	<!-- <a href="{{ URL::route('registro.anunciante') }}" -->
	   <!-- class="_container clearfix"> -->
		<!-- <button class="_button">Crear Anuncio</button> -->
	<!-- </a> -->
	<div class="_container clearfix">
		<input type="submit" class="_button" value="Crear Anuncio">
	</div>
	{{ Form::close() }}
</div>
</body>
</html>
