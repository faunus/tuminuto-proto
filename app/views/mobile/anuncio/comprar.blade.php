<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0">
	<title>buy</title>
	<link href="http://fonts.googleapis.com/css?family=Noto+Sans:400,400" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/test/mobile/css/standardize
	.css">
	<link rel="stylesheet" href="/test/mobile/css/buy-grid.css">
	<link rel="stylesheet" href="/test/mobile/css/buy.css">
</head>
<body class="body buy clearfix">
<nav class="igator clearfix">
	<img class="logo" src="/images/logo.png">
</nav>
<div class="buy buy-1 clearfix">
	<label class="ad-location ad-location-1">UBICACIÓN</label>
	<div class="ad-location ad-location-2">
		<p>Queretaro</p>
		<p>Centro</p>
	</div>
	<label class="ad-schedule ad-schedule-1">HORARIO</label>
	<div class="ad-schedule ad-schedule-2">
		<p>Lunes a Sábado</p>
		<p>16:00 a 20:00</p>
	</div>
	<label class="ad-duration ad-duration-1">DURACIÓN</label>
	<p class="ad-duration ad-duration-2">1 Mes</p>
	<a href="{{ URL::route('comprado.anuncio') }}"
	   class="_container
	clearfix">
		<button class="buy-btn" type="submit">Comprar</button>
	</a>
</div>
</body>
</html>
