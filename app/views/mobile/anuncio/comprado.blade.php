<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0">
	<title>buy-success</title>
	<link href="http://fonts.googleapis.com/css?family=Noto+Sans:400,400" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/test/mobile/css/standardize
	.css">
	<link rel="stylesheet" href="/test/mobile/css/buy-success-grid.css">
	<link rel="stylesheet" href="/test/mobile/css/buy-success.css">
</head>
<body class="body buy-success clearfix">
<nav class="igator clearfix">
	<img class="logo" src="/images/logo.png">
</nav>
<h2 class="_text-1">COMPRA EXITOSA</h2>
<p class="text">Tu anuncio fue agregado al punto de promoción</p>
<a class="_container clearfix" href="{{ URL::route
('titino') }}">
	<button class="_button">Ver Anuncio</button>
</a>
</body>
</html>
