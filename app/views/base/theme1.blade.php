<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="tu minuto">
    <meta name="author" content="">

    <title>~ tu minuto ~</title>

    <!-- Stylesheets -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animation.css" rel="stylesheet">
    <link href="css/checkbox/orange.css" rel="stylesheet">
    <link href="css/preview.css" rel="stylesheet">
    <link href="css/authenty.css" rel="stylesheet">
<!--    outer style-->
    <link rel="stylesheet" media="screen" href="css/style.css"/>
    <style>
        body{
            color: #FFFFFF;
        }
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 999px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>

    <!-- Font Awesome CDN -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--<nav id="navigation_menu">-->
<!--    <ul class="clearfix">-->
<!--        <li><a href="#signin_main">1</a></li>-->
<!--        <li><a href="#signin_alt">2</a></li>-->
<!--        <li><a href="#signup_window">3</a></li>-->
<!--        <li><a href="#signup_wizard">4</a></li>-->
<!--        <li><a href="#password_recovery">5</a></li>-->
<!--        <li><a href="#preview_cta">6</a></li>-->
<!--    </ul>-->
<!--</nav>-->
@if(Session::has('errors'))
<div id="alert-error" class="alert alert-danger" style="display: none;">
    @foreach( $errors->all() as $e )
    <li>{{ $e }}</li>
    @endforeach
</div>
@endif

<!-- main form-->
@include('chunks.main2')


<!-- js library -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.icheck.min.js"></script>
<script src="js/waypoints.min.js"></script>

<!-- authenty js -->
<script src="js/authenty.js"></script>

<!--<script src="js/supersized.3.2.7.min.js"></script>-->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/gmaps.js"></script>
<!--<script type="text/javascript" src="js/plugins/retina-1.1.0.js"></script>-->
<!--<script type="text/javascript" src="js/Horrus.js"></script>-->


<!-- preview scripts -->
<script src="js/preview/jquery.malihu.PageScroll2id.js"></script>
<script src="js/preview/jquery.address-1.6.min.js"></script>
<script src="js/preview/scrollTo.min.js"></script>
<script src="js/preview/init.js"></script>

<!-- preview scripts -->
<script>
    (function($) {

        var tm = {}; //tu-minuto

        tm.conf = { // configuration
            duration: 800
        };

        tm.cache = {
            $alertError: $('#alert-error')
        };

        tm.cache.$alertError.fadeIn(tm.conf.duration);

        // get full window size
        $(window).on('load resize', function(){
            var w = $(window).width();
            var h = $(window).height();

            $('section').height(h);
        });

        // scrollTo plugin
        $('#signup_from_1').scrollTo({ easing: 'easeInOutQuint', speed: 1500 });
        $('#forgot_from_1').scrollTo({ easing: 'easeInOutQuint', speed: 1500 });
        $('#signup_from_2').scrollTo({ easing: 'easeInOutQuint', speed: 1500 });
        $('#forgot_from_2').scrollTo({ easing: 'easeInOutQuint', speed: 1500 });
        $('#forgot_from_3').scrollTo({ easing: 'easeInOutQuint', speed: 1500 });


        // set focus on input
        var firstInput = $('section').find('input[type=text], input[type=email]').filter(':visible:first');

        if (firstInput != null) {
            firstInput.focus();
        }

        $('section').waypoint(function (direction) {
            var target = $(this).find('input[type=text], input[type=email]').filter(':visible:first');
            target.focus();
        }, {
            offset: 300
        }).waypoint(function (direction) {
            var target = $(this).find('input[type=text], input[type=email]').filter(':visible:first');
            target.focus();
        }, {
            offset: -400
        });


        // animation handler
        $('[data-animation-delay]').each(function () {
            var animationDelay = $(this).data("animation-delay");
            $(this).css({
                "-webkit-animation-delay": animationDelay,
                "-moz-animation-delay": animationDelay,
                "-o-animation-delay": animationDelay,
                "-ms-animation-delay": animationDelay,
                "animation-delay": animationDelay
            });
        });

        $('[data-animation]').waypoint(function (direction) {
            if (direction == "down") {
                $(this).addClass("animated " + $(this).data("animation"));
            }
        }, {
            offset: '90%'
        }).waypoint(function (direction) {
            if (direction == "up") {
                $(this).removeClass("animated " + $(this).data("animation"));
            }
        }, {
            offset: '100%'
        });

    })(jQuery);

//    jQuery(function ($) {
//        $('#home-section-wrapper').supersized({
//        $('#password_recovery').supersized({
//            slide_interval : 7666, // Length between transitions
//            transition : 1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
//            transition_speed : 900, // Speed of transition
//            slide_links : 'name', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
//            slides : [
//                {image : 'images/bici.jpg', title : '', thumb : '', url : ''},
//                {image : 'images/futbolito.jpg', title : '', thumb : '', url : ''},
//                {image : 'images/sueter.jpg', title : '', thumb : '', url : ''}
//            ]
//        });
//    });

    /* ==========================================================================
     Map
     ========================================================================== */
    var fau_lat = 20.6396439;
    var fau_lon = -100.4583989;

    // get info
    // http://maps.google.com/maps/api/geocode/json?address=12+de+Diciembre+queretaro&sensor=false

    map = new GMaps({
        zoom: 13,
        el: '#map',
        scrollwheel: false,
        lat: fau_lat,
        lng: fau_lon
    });
    map.addMarker({
        lat: fau_lat,
        lng: fau_lon,
        icon: "images/marker.png"
    });

    // jurica
    map.addMarker({
        lat: 20.6490506,
        lng: -100.4393293,
        icon: "images/marker.png"
    });

    // valle de los olivos
    map.addMarker({
        lat: 20.5491071,
        lng: -100.4140209,
        icon: "images/marker.png"
    });

    // san pablo
    map.addMarker({
        lat: 20.6219949,
        lng: -100.40735,
        icon: "images/marker.png"
    });

    // El Campanario
    map.addMarker({
        lat: 20.6182789,
        lng: -100.3469826,
        icon: "images/marker.png"
    });

    // el pedregal
    map.addMarker({
        lat: 20.6067154,
        lng: -100.3740025,
        icon: "images/marker.png"
    });

    // Ex-Hacienda El Tintero
    map.addMarker({
        lat: 20.6068321,
        lng: -100.4398098,
        icon: "images/marker.png"
    });

    // Jardines de La Hacienda
    map.addMarker({
        lat: 20.5745579,
        lng: -100.4129676,
        icon: "images/marker.png"
    });

    // san pablo
    map.addMarker({
        lat: 20.5969271,
        lng: -100.3868135,
        icon: "images/marker.png"
    });


</script>


</body>
</html>
