<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="tu minuto">
    <meta name="author" content="">

    <title>~ tu minuto ~</title>

    <!-- Stylesheets -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/animation.css" rel="stylesheet">
    <link href="/css/checkbox/orange.css" rel="stylesheet">
    <link href="/css/preview.css" rel="stylesheet">
    <link href="/css/authenty.css" rel="stylesheet">
    <!--    outer style-->
    <link rel="stylesheet" media="screen" href="/css/style.css"/>
    <style>
        body{
            color: #FFFFFF;
        }
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 999px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        * {
            box-sizing: border-box;
        }

        .container {

            width: 90%;
            margin-bottom: 40px;
        }

        .item {
            margin-bottom: 20px;
            /*width:  40%;*/
            /*height: 60px;*/
            /*float: left;*/
            border: 2px solid;
            /*background: #09F;*/
        }
    </style>

    <!-- Font Awesome CDN -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--<nav id="navigation_menu">-->
<!--    <ul class="clearfix">-->
<!--        <li><a href="#signin_main">1</a></li>-->
<!--        <li><a href="#signin_alt">2</a></li>-->
<!--        <li><a href="#signup_window">3</a></li>-->
<!--        <li><a href="#signup_wizard">4</a></li>-->
<!--        <li><a href="#password_recovery">5</a></li>-->
<!--        <li><a href="#preview_cta">6</a></li>-->
<!--    </ul>-->
<!--</nav>-->
@if(Session::has('errors'))
<div id="alert-error" class="alert alert-danger" style="display: none;">
    @foreach( $errors->all() as $e )
    <li>{{ $e }}</li>
    @endforeach
</div>
@endif

<!-- main form-->
@yield('body')


<!-- js library -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/masonry.min.js"></script>

<script>
    $(document).ready(function(){
        var container = document.querySelector('#basic');
        var msnry = new Masonry( container, {
            columnWidth: 100,
            itemSelector: '.item'
        });
    }
</script>
</body>
</html>
