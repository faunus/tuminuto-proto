<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>~Tu Minuto~</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            text-align:center;
            color: #999;
        }

        .welcome {
            width: 300px;
            height: 200px;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -150px;
            margin-top: -100px;
        }

        a, a:visited {
            text-decoration:none;
        }

        h1 {
            font-size: 32px;
            margin: 16px 0 0 0;
        }
    </style>
</head>
<body>
<div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://js.pusher.com/2.1/pusher.min.js" type="text/javascript"></script>

    @if(Session::has('errors'))
        <div id="alert-error" class="alert alert-danger" style="display: none;">
            @foreach( $errors->all() as $e )
                <li>{{ $e }}</li>
            @endforeach
        </div>
    @endif

    @yield("test")


    <script>
        var tm = {}; //tu-minuto

        tm.conf = { // configuration
            duration: 800
        };

        tm.cache = {
          $alertError: $('#alert-error')
        };

        tm.cache.$alertError.fadeIn(tm.conf.duration);
    </script>
</div>
</body>
</html>
