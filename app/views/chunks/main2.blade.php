<!-- =========================================
       Home Section
       ========================================== -->
<!-- home-section-wrapper -->

<section id="authenty_preview">

                        <section id="password_recovery" class="authenty password-recovery">
                            <div class="section-content">
                                <div class="wrap">
                                    <div class="container">
                                        <div class="form-wrap">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3 brand" data-animation="fadeInUp">
                                                    <!--                                <h2>TuMinuto</h2>-->
                                                    <h2 style="color: #ffffff; font-weight: 200;">Compra y venta express en tu colonia</h2>
                                                </div>
                                                <div class="col-sm-1 hidden-xs">
                                                    <div class="horizontal-divider"></div>
                                                </div>
                                                <div class="col-xs-12 col-sm-8 main" data-animation="fadeInLeft" data-animation-delay=".5s">
                                                    <h2>Tu Minuto.</h2>
                                                    <!--                                <p>A veces uno no se da cuenta de lo que tu vecindario tiene para ofrecerte.</p>-->

                                                    {{ Form::open([
                                                    "route"     => "anuncio.post",
                                                    "method"    => "post",
                                                    "files"     => true
                                                    ])}}
                                                    <div class="form-group">
                                                            <span class="btn btn-file btn-block fau-file">
                                                                Escoger Imagen
                                                                {{ Form::file("ad_image") }}
                                                            </span>
                                                    </div>
                                                    <br/> <!--- lista de titinos disponibles -->
                                                    <div class="form-group">
                                                        {{ Form::select('ad_titino', $titinos_list, null,
                                                        ["class" => "form-control"]) }}
                                                    </div>
                                                    <br/>
                                                    <div class="form-group">
                                                        {{ Form::select('ad_time', $timekinds_list, null,
                                                        ["class" => "form-control"]) }}
                                                    </div>
                                                    <br/>
                                                    <div class="form-group">
                                                        {{ Form::select('ad_kind', $adkinds_list, null,
                                                        ["class" => "form-control"]) }}
                                                    </div>
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4 col-sm-offset-8">
                                                            {{ Form::submit("Anunciate",
                                                            ["class" => "btn btn-block reset" ]) }}
                                                        </div>
                                                    </div>

                                                    {{ Form::close() }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>

<!-- /home-section-wrapper -->

<!-- main form-->

<div class="wrap">
    @include('chunks.ver-canal-form')
</div>


<!-- =========================================
      Map Section
      ========================================== -->
<!-- map-section -->
<section id="map-section">

    <!-- map -->
    <div id="map"></div>

</section><!-- /map-section -->
