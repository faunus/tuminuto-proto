@extends("base.minimo")

@section("body")
    <section>
        <h1 class="text-center" style="margin-top: 100px; color: #ffffff;">
            Anuncios Actuales en {{ $colonia }}, Querétaro.
        </h1>
        <br/>
        <div id="basic" class="container">
            @foreach( $anuncios as $ima )
            <div class="item">
                <img width="60%" src="/tuminuto/{{ $ima->path }}" alt=""/>
            </div>
            @endforeach
        </div>
    </section>
@stop