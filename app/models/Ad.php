<?php

class Ad extends \Eloquent {
	protected $fillable = ['name', 'email'];
    protected $table = "ad";
}