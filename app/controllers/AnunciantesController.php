<?php

class AnunciantesController extends AnuncianteStuff {

	public function registro()
	{
		if(Agent::isMobile())
		{
			return View::make('mobile.anunciante.registro');
		}

		return View::make('anunciante.registro');
	}

    public function postRegistro()
	{
		if($this->AnuncianteCreado() && Auth::check())
        {
            return Redirect::route('home.anunciante');
        }

        return Redirect::route('home');
	}

    public function home()
    {
        return 'home';
    }

}