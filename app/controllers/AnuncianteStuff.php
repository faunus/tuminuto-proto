<?php

class AnuncianteStuff extends \BaseController {

    public function AnuncianteCreado()
    {
        $input  = Input::all();
        $rules  = [
            'name'     => 'required',
            'phone'    => 'required|numeric',
            'password' => 'required',
            'email'    => 'required|email|unique:anunciantes,email',
        ];
        $messages = [
            'name.required'     => 'Nombre requerido',
            'phone.required'    => 'Teléfono requerido',
            'phone.numeric'     => 'Tel No es Número',
            'phone.between'     => 'Tel No Válido',
            'password.required' => 'Contraseña requerida',
            'email.required'    => 'Correo Requerido',
            'email.email'       => 'Correo No Válido',
            'email.unique'      => 'Correo Ya Registrado',
        ];

        $validator = Validator::make( $input, $rules, $messages );

        if ( $validator->fails() )
        {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();

        }

        return $this->crearAnunciante($input);
    }

    public function iniciarSesionAnunciante(array $data)
    {
        return true;
    }

    public function crearAnuncio(array $anuncioData)
    {
        return $anuncioData;
    }

    public function buscarAnuncio($palabra_clave)
    {
        return $palabra_clave;
    }

    public function verAnuncios()
    {
        return 'regresar paginado';
    }

    /**
     * @param $input
     */
    protected function crearAnunciante($input)
    {
        try
        {
            $user = new User;
            $user->name = $input['name'];
            $user->email = $input['email'];
            $user->password = Hash::make($input['password']);
            $user->save();

            $anunciante = new Anunciante;
            $anunciante->user_id = $user->id;
            $anunciante->email = $user->email;
            $anunciante->save();
        }
        catch (\Exception $e)
        {
            //todo: notificar
            return false;
        }

        Auth::login($user, true);

        return true;
    }
}