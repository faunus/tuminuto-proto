<?php

class PusherController extends \BaseController {

	public function polaco()
	{
        return View::make('test.pusher');
	}

    public function pusher()
    {
        $app_id = getenv('pusher_id');
        $app_key = getenv('pusher_key');
        $app_secret = getenv('pusher_secret');

        $duracion = 20;
        $filename = "tuminuto/futbolito.jpg";
        $pusher = new Pusher( $app_key, $app_secret, $app_id );
        $pusher->trigger('titino_anuncio', 'nuevo_anuncio', array('url' => $filename, 'duration' => $duracion), null, true );
        var_dump($pusher);
    }

}