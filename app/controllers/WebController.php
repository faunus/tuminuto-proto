<?php

class WebController extends \BaseController {

	public function home()
	{
		$opciones_titinos = ["null" => "Ubicación"] +
				DB::table('titinos')
						->join('location', 'titinos.location_id', '=', 'location.id')
						->select('location.slug as id', 'location.name as kind')
						->orderBy('kind')
						->lists('kind', 'id');

		$opciones_tiempos = ["null" => "- elige tu tiempo -"] +
				DB::table('timeskind')->lists('kind', 'id');

		$opciones_ad = ["null" => "- elige el tipo de anuncio -"] +
				DB::table('adkind')->lists('kind', 'id');

		if(Agent::isMobile())
		{
			return View::make('mobile.web.home')
				->with("titinos_list", $opciones_titinos);;
		}

    return View::make('base.theme1')
        ->with("timekinds_list", $opciones_tiempos)
        ->with("adkinds_list", $opciones_ad)
        ->with("titinos_list", $opciones_titinos);
	}

	public function homePost()
	{

		// hardcoded for fake demo
		$input  = Input::all();

		$rules  = [
				'ad_image'     => 'required',
				// 'ad_kind'      => 'required|numeric',
				// 'ad_location'    => 'required',
		];

		$messages = [
				'ad_image.required' => 'Olvidaste subir la imagen',
				// 'ad_image.image'    => 'Tu archivo no es de tipo imagen',
				'ad_time.numeric'   => 'No selecionaste tiempo',
				'ad_kind.numeric'   => 'No selecionaste tipo',
				//        'ad_titino.exists'  => 'No selecionaste ubicación para tu anuncio',
				'ad_kind.max'       => 'No selecionaste una opción inválida',
		];

		$validator = Validator::make( $input, $rules, $messages );

		// take the image
		if ( ! Input::hasFile('ad_image'))
		{
				return Redirect::back()
						->withErrors($validator->getMessageBag())
						->withInput();
		}
		else
		{
				$imagen             = Input::file('ad_image');
				$destination_path   = public_path() .'/tuminuto';
				$filename           = "tm_" .  Str::random(8) . "." . $imagen->getClientOriginalExtension();
				$imagen->move( $destination_path, $filename );
		}

		if ( $validator->fails() )
		{
				return Redirect::back()
						->withErrors($validator->getMessageBag())
						->withInput();
		}

		// take the location

		$location_id = (int) 999; // set to titino page


		// take the kind

		$duration = 3; // hardcoded 10 seconds

		$ad = new Ad;
		$ad->user_id        =  1; // titino user hardcoded
		$ad->path           =  $filename;
		$ad->date           =  \Carbon\Carbon::now();
		$ad->location_id    = $location_id;
		$ad->duration    	 = (int) $duration;
		$ad->save();

		$app_id = getenv('pusher_id');
		$app_key = getenv('pusher_key');
		$app_secret = getenv('pusher_secret');

		$url = "tuminuto/" . $filename;

		// pusher stuff
		$pusher = new Pusher( $app_key, $app_secret, $app_id );
		$pusher->trigger('titino_anuncio', 'nuevo_anuncio', array('url' => $url, 'duration' => $duration), null, true );

		if(Agent::isMobile())
		{
			return View::make('mobile.anunciante.registro');
		}

		return Redirect::route('titino');
	}

	public function titino()
	{
        $fractal = new League\Fractal\Manager();

        $ads = Ad::where('location_id', 999)->get()->toArray();

        $resource = new League\Fractal\Resource\Collection( $ads, function(array $ads)
        {
            return [
                'url'      => URL::asset('tuminuto') . "/" . $ads['path'],
                'seconds'  => (int) $ads['duration'],
            ];
        });

        $json = $fractal->createData( $resource )->toJson();

        return View::make('test.pawel')->with('huu', $json);
	}

    public function anuncio()
    {

				// hardcoded for fake demo
				$input  = Input::all();

				$rules  = [
						'ad_image'     => 'required',
						// 'ad_kind'      => 'required|numeric',
						// 'ad_location'    => 'required',
				];
				$messages = [
						'ad_image.required' => 'Olvidaste subir la imagen',
						// 'ad_image.image'    => 'Tu archivo no es de tipo imagen',
						'ad_time.numeric'   => 'No selecionaste tiempo',
						'ad_kind.numeric'   => 'No selecionaste tipo',
						//        'ad_titino.exists'  => 'No selecionaste ubicación para tu anuncio',
						'ad_kind.max'       => 'No selecionaste una opción inválida',
				];

				$validator = Validator::make( $input, $rules, $messages );

				// take the image
				if ( ! Input::hasFile('ad_image'))
				{
						return Redirect::back()
								->withErrors($validator->getMessageBag())
								->withInput();
				}
				else
				{
						$imagen             = Input::file('ad_image');
						$destination_path   = public_path() .'/tuminuto';
						$filename           = "tm_" .  Str::random(8) . "." . $imagen->getClientOriginalExtension();
						$imagen->move( $destination_path, $filename );
				}

				if ( $validator->fails() )
				{
						return Redirect::back()
								->withErrors($validator->getMessageBag())
								->withInput();
				}

				// take the location

				$location_id = (int) 999; // set to titino page


				// take the kind

				$duration = 3; // hardcoded 10 seconds

				$ad = new Ad;
				$ad->user_id        =  1; // titino user hardcoded
				$ad->path           =  $filename;
				$ad->date           =  \Carbon\Carbon::now();
				$ad->location_id    = $location_id;
				$ad->duration    	 = (int) $duration;
				$ad->save();

				$app_id = getenv('pusher_id');
				$app_key = getenv('pusher_key');
				$app_secret = getenv('pusher_secret');

				$url = "tuminuto/" . $filename;

				// pusher stuff
				$pusher = new Pusher( $app_key, $app_secret, $app_id );
				$pusher->trigger('titino_anuncio', 'nuevo_anuncio', array('url' => $url, 'duration' => $duration), null, true );

				if(Agent::isMobile())
				{
					return View::make('mobile.anunciante.registro');
				}

				return Redirect::route('titino');
    }

    public function verCanal()
    {
        if( Input::get('titino_slug') === "titino" )
        {
            return Redirect::route('canal.get', ["titino"]);
        }

        // find id number
        $canal = DB::table('location')
            ->where('slug', '=', Input::get('titino_slug'))
            ->first(['id', 'slug']);

        if ( is_null($canal) )
        {
            return Redirect::back();
        }

        return Redirect::route('canal.get', [$canal->slug]);
    }

    public function canal($colonia)
    {
        if( $colonia !== "titino" )
        {
            // find id number
            $canal = DB::table('location')
                ->where('slug', '=', $colonia)
                ->first(['id', 'name']);

            $colonia_nombre = $canal->name;


            // verify if exists
            $exists = DB::table('titinos')
                ->where('location_id', '=', (int)$canal->id)
                ->first();

            if ( is_null($exists) )
            {
                return "no hay";
            }

            // get ads by location
            $data = Ad::where('location_id', '=', (int)$canal->id)
                ->get();
        }
        else
        {
            $colonia_nombre = "titino";
            $data = Ad::where('location_id', '=', 999)
                ->get();
        }

        if ( $data->isEmpty() )
        {
            return "no hay anuncios";
        }

        return View::make('canales.page')
            ->with('anuncios', $data)
            ->with('colonia', $colonia_nombre);
    }
}
