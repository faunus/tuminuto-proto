<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        // if ( App::environment() === "production" )
        // {
        //     throw new Exception("do not do this at production idiot!!!");
        // }
		Eloquent::unguard();

		 $this->call('TimeskindTableSeeder');
		 $this->call('AdkindTableSeeder');
		 $this->call('LocationTableSeeder');
		 $this->call('TitinosTableSeeder');
		 $this->call('AdTableSeeder');
	}

}
