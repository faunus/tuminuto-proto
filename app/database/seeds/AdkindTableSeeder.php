<?php

class AdkindTableSeeder extends Seeder {

	public function run()
	{
        DB::table('adkind')->truncate();

        $tipos = [
            "adultos",
            "deportivo",
            "electrónicos",
            "videojuegos",
            "instrumentos musicales",
            "libros",
            "juguetes infantiles",
            "etc"
        ];
        foreach($tipos as $tipo)
        {
            DB::table('adkind')->insert([
                'kind'          => $tipo,
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now()
            ]);
        }
	}

}