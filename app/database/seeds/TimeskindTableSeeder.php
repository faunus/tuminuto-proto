<?php

class TimeskindTableSeeder extends Seeder {

	public function run()
	{
        DB::table('timeskind')->truncate();

		foreach(range(1, 6) as $index)
		{
			DB::table('timeskind')->insert([
                'kind'          => $index . "0 segundos",
                'duration'      => 3,
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now()
            ]);
		}
	}

}
