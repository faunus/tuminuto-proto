<?php

use Faker\Factory as Faker;

class AdTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
        $location_titinos = DB::table('titinos')
            ->select('id', 'location_id')
            ->lists('location_id');

        $path = __DIR__ . '/../../../public/tuminuto';

		foreach(range(1, 10) as $index)
		{
			Ad::create([
          'user_id'       => rand(100, 200),
          'path'          => $faker->image($path, 640, 480, false, false),
          'date'          => \Carbon\Carbon::now(),
          'duration'      => 3,
          'location_id'   => $faker->randomElement($location_titinos)
			])->touch();
		}
	}

}
