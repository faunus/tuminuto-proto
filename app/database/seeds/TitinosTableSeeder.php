<?php

class TitinosTableSeeder extends Seeder {

	public function run()
	{

        $table = 'titinos';
        $max_location = DB::table('location')->count();
        DB::table($table)->truncate();

		foreach(range(1, 14) as $index)
		{
            DB::table($table)->insert([
                'user_id'       => $index,
                'location_id'   => rand(1,$max_location),
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now(),
            ]);
		}

        DB::table($table)->insert([
            'user_id'       => (int)15,
            'location_id'   => 999,
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now(),
        ]);
	}

}